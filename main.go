package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"

	"github.com/joho/godotenv"
	_ "github.com/lib/pq"

	"github.com/gorilla/mux"
)

var indexTemplate = template.Must(template.ParseFiles("routes/index.html"))
var successTemplate = template.Must(template.ParseFiles("routes/success.html"))

// Структура для хранения записи из БД
type Appointment struct {
	Time string `json:"time"`
}

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatalf("Ошибка загрузки файла .env: %v", err)
	}

	InitDB()
	defer CloseDB()

	// Добавление таблицы со всеми приемами
	CreateAppointmentsTable(DB)

	r := mux.NewRouter()

	// Обрабатываем папку static для статический файлов по типу css, js, image...
	staticFileDirectory := http.Dir("./static/")
	staticFileHandler := http.StripPrefix("/static/", http.FileServer(staticFileDirectory))
	r.PathPrefix("/static/").Handler(staticFileHandler)

	// Все маршруты приложения
	r.HandleFunc("/", HomeHandler).Methods("GET")             // Главная страница
	r.HandleFunc("/success", SuccessHandler).Methods("GET")   // Страница с сообщением о успешной записью
	r.HandleFunc("/hours_data", hoursData(DB)).Methods("GET") // Страница для вывода статусов занятости времени

	r.HandleFunc("/appointment", func(w http.ResponseWriter, r *http.Request) {
		AppointmentHandler(w, r, DB)
	}).Methods("POST") // Метод для добавления новой записи

	http.Handle("/", r)
	fmt.Println("Сервер запущен на порту 8080")
	log.Fatal(http.ListenAndServe(":8081", nil))
}

// Главная на странице
func HomeHandler(w http.ResponseWriter, r *http.Request) {
	if err := indexTemplate.ExecuteTemplate(w, "index.html", nil); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

// Страница успешной записи
func SuccessHandler(w http.ResponseWriter, r *http.Request) {
	if err := successTemplate.ExecuteTemplate(w, "success.html", nil); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

// Вывод всех записей по дням
func hoursData(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		queryValues := r.URL.Query()
		date := queryValues.Get("date")

		if date == "" {
			http.Error(w, "Missing required query parameter 'date'", http.StatusBadRequest)
			return
		}

		// SQL запрос для получения данных по указанной дате
		query := `SELECT time FROM appointments WHERE date LIKE '%' || $1 || '%'`
		rows, err := db.Query(query, date)
		if err != nil {
			log.Printf("Failed to execute query: %v", err)
			http.Error(w, "Failed to execute query", http.StatusInternalServerError)
			return
		}
		defer rows.Close()

		var times []string
		for rows.Next() {
			var time string
			if err := rows.Scan(&time); err != nil {
				log.Printf("Failed to scan row: %v", err)
				http.Error(w, "Failed to scan row", http.StatusInternalServerError)
				return
			}
			times = append(times, time)
		}

		// Проверка ошибки после итерации по строкам
		if err := rows.Err(); err != nil {
			log.Printf("Rows iteration error: %v", err)
			http.Error(w, "Error iterating over rows", http.StatusInternalServerError)
			return
		}

		// Преобразование результата в JSON
		jsonData, err := json.Marshal(times)
		if err != nil {
			log.Printf("Failed to encode JSON: %v", err)
			http.Error(w, "Failed to encode JSON", http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.Write(jsonData)
	}
}

// Добавление новой записи на прием в БД
func AppointmentHandler(w http.ResponseWriter, r *http.Request, db *sql.DB) {
	// Проверяем, что это POST запрос
	if r.Method != http.MethodPost {
		http.Error(w, "Invalid request method", http.StatusMethodNotAllowed)
		return
	}

	// Парсим форму
	err := r.ParseMultipartForm(1024) // 1024 - это максимальный размер в байтах, выделяемый для памяти
	if err != nil {
		http.Error(w, "Error parsing form", http.StatusBadRequest)
		return
	}

	// Извлекаем данные из формы
	date := r.FormValue("date")
	time := r.FormValue("time")
	name := r.FormValue("name")
	surname := r.FormValue("surname")

	// Здесь вы могли бы добавить логику сохранения данных в базу данных
	query := `INSERT INTO appointments (date, time, name, surname) VALUES ($1, $2, $3, $4)`

	// TODO: сделать нотификейшн в ТГ при добавление новой записи

	_, err = db.Exec(query, date, time, name, surname)
	if err != nil {
		log.Fatal(err)
	}

	// Отправляем ответ
	http.Redirect(w, r, "/success", http.StatusSeeOther)
}
