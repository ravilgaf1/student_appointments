package main

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"strconv"

	_ "github.com/lib/pq"
)

var (
	DB *sql.DB
)

// InitDB инициализирует подключение к базе данных
func InitDB() {
	// Получаем переменные окружения
	dbHost := os.Getenv("POSTGRES_HOST")
	dbPort := os.Getenv("POSTGRES_PORT")
	dbUser := os.Getenv("POSTGRES_USER")
	dbPass := os.Getenv("POSTGRES_PASS")
	dbName := os.Getenv("POSTGRES_NAME")

	port, err := strconv.Atoi(dbPort)
	if err != nil {
		log.Fatalf("Invalid port value: %v", err)
	}

	connStr := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", dbHost, port, dbUser, dbPass, dbName)

	DB, err = sql.Open("postgres", connStr)
	if err != nil {
		log.Fatal(err)
	}

	// Проверка что все работает
	err = DB.Ping()
	if err != nil {
		log.Fatal(err)
	}
}

// CloseDB закрывает подключение к базе данных
func CloseDB() {
	if DB != nil {
		DB.Close()
	}
}

// CreateAppointmentsTable добавляет новую таблицу записей на прием
func CreateAppointmentsTable(db *sql.DB) {
	query := `
    CREATE TABLE IF NOT EXISTS appointments (
        id SERIAL PRIMARY KEY,
        date VARCHAR(255),
        time VARCHAR(255),
        name VARCHAR(255),
        surname VARCHAR(255)
    )`

	_, err := db.Exec(query)
	if err != nil {
		log.Fatal(err)
	}
}
