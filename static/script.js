document.addEventListener("DOMContentLoaded", () => {
	let inputDate = document.getElementById("inputDate");
	let inputTime = document.getElementById("inputTime");

	const scheduleContainer = document.getElementById('schedule-container');
	const days = ['среда', 'четверг'];
	const startTime = "18:00";
	const endTime = "19:00";
	const slotDuration = 5; // in minutes

	// Функция вычисления ближайшей даты указанного дня недели
	function getNextDateOfWeek(dayOfWeek) {
		const today = new Date();
		today.setHours(0, 0, 0, 0); // Обнуляем время, чтобы сравнение дат не зависело от времени дня.

		const dayIndex = ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'].indexOf(dayOfWeek.toLowerCase());

		if (dayIndex === -1) {
			throw new Error('Invalid day of week: ' + dayOfWeek);
		}

		const currentDayIndex = today.getDay();
		let daysUntilNext = (dayIndex - currentDayIndex + 7) % 7;

		// Если вычислено "0 дней", значит сегодня уже этот день недели, и следующий будет только через неделю.
		if (daysUntilNext === 0) {
			daysUntilNext = 7;
		}

		const nextDate = new Date(today.getTime());
		nextDate.setDate(today.getDate() + daysUntilNext); // Изменяем дату объекта today на ближайший день недели.

		// Форматируем дату в нужном формате dd.mm.yyyy
		const formattedDate = `${String(nextDate.getDate()).padStart(2, '0')}.${String(nextDate.getMonth() + 1).padStart(2, '0')}.${nextDate.getFullYear()}`;

		return { dayOfWeek, formattedDate, nextDate };
	}

	// Создание слотов времени (разбиваете интервал на части)
	function createTimeSlots(startTime, endTime, slotDuration) {
		const slots = [];
		let [startHours, startMinutes] = startTime.split(':').map(Number);
		let [endHours, endMinutes] = endTime.split(':').map(Number);

		while (startHours * 60 + startMinutes < endHours * 60 + endMinutes) {
			slots.push(`${String(startHours).padStart(2, '0')}:${String(startMinutes).padStart(2, '0')}`);
			startMinutes += slotDuration;
			if (startMinutes >= 60) {
				startHours += 1;
				startMinutes -= 60;
			}
		}

		return slots;
	}

	// Основная функция генерации расписания
	function generateSchedule(days, startTime, endTime, slotDuration) {
		// Массив для хранения дат
		const dates = days.map(day => getNextDateOfWeek(day));

		// Сортировка ближайших дат по возрастанию
		dates.sort((a, b) => a.nextDate - b.nextDate);

		// Генерация расписания
		dates.forEach(async ({ dayOfWeek, formattedDate }) => {
			const dayItem = document.createElement('div');
			dayItem.className = 'day__item';

			const response = await fetch(`http://localhost:8081/hours_data?date=${formattedDate}`);
			const data = await response.json();

			const strong = document.createElement('strong');
			strong.innerText = `${dayOfWeek.charAt(0).toUpperCase() + dayOfWeek.slice(1)} (${formattedDate})`;
			dayItem.appendChild(strong);

			const buttonContainer = document.createElement('div');
			const timeSlots = createTimeSlots(startTime, endTime, slotDuration);

			timeSlots.forEach((time) => {
				const button = document.createElement('button');
				button.setAttribute('data-time', time);
				button.innerText = time;

				if (data && data.find((element) => element === time)) {
					button.disabled = true;
				}
				
				button.addEventListener('click', (event) => selectDay(event));
				buttonContainer.appendChild(button);
			});

			dayItem.appendChild(buttonContainer);
			scheduleContainer.appendChild(dayItem); // Assuming you have a container with id 'scheduleContainer'
		});
	}

	function selectDay(event) {
		const buttons = document.querySelectorAll('button[data-time]');
		buttons.forEach(btn => btn.classList.remove('active'));
		event.target.classList.add('active');

		const selectedTime = event.target.dataset.time;
		const selectedDay = event.target.closest('.day__item').querySelector('strong').innerText;

		inputDate.value = selectedDay;
		inputTime.value = selectedTime;
	}


	generateSchedule(days, startTime, endTime, slotDuration);



	// const dates = days.map(day => getNextDateOfWeek(day));
	// Получаем от сервера статусы времен
	// (async (url1, url2) => {
	// 	try {

	// 		// Параллельный запуск двух fetch запросов
	// 		const [response1, response2] = await Promise.all([
	// 			fetch(url1),
	// 			fetch(url2)
	// 		]);

	// 		// Преобразование ответов в JSON
	// 		const [data1, data2] = await Promise.all([
	// 			response1.json(),
	// 			response2.json()
	// 		]);

	// 		// Вывод данных в консоль
	// 		console.log('Data 1:', data1);
	// 		console.log('Data 2:', data2);
	// 	} catch (error) {
	// 		console.error('Ошибка:', error);
	// 	}
	// })(
	// 	`http://localhost:8081/hours_data?date=${dates[0].formattedDate}`,
	// 	`http://localhost:8081/hours_data?date=${dates[1].formattedDate}`
	// );
});
